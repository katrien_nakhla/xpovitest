import React, { useState, useRef, useContext, useEffect } from "react";
import gsap from "gsap";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { Link } from "react-router-dom";
import arrowright from "../../Assets/Images/arrow-right.png";
import Progressbar from "../../Compnents/ProgressBar";
import { FormContxt } from "../../App";
import "./Questions.css";

export default function Questions3() {
  let Logo = useRef(null);
  const { stepNum, questionsAns, handleStepNumber, handlequestionsAns } =
    useContext(FormContxt);
  const [ErrorFlag, setErrorFlag] = useState(
    questionsAns[2].Q3 === null ? true : false
  );
  

  return (
    <>
      <div className="questionContent">
        <h1 className="font25">Do you target all industries?</h1>
      </div>
      <Container className="flexEvenly">
        <Row>
          <Col
            lg="6"
            md="12"
            className="mt-2"
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <div
              className={
                questionsAns[2].Q3 === "Yes"
                  ? "singleOptionActive"
                  : "singleOption"
              }
              onClick={() => {
                handlequestionsAns("Yes", 3);
                setErrorFlag(false);
              }}
            >
              <h3>Yes</h3>
            </div>
          </Col>
          <Col
            lg="6"
            md="12"
            className="mt-2"
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <div
              className={
                questionsAns[2].Q3 === "No"
                  ? "singleOptionActive"
                  : "singleOption"
              }
              onClick={() => {
                handlequestionsAns("No", 3);
                setErrorFlag(false);
              }}
            >
              <h3>No</h3>
            </div>
          </Col>
        </Row>
      </Container>
    </>
  );
}
