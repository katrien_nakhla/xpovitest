import React, { useState, useRef, useContext, useEffect } from "react";
import gsap from "gsap";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { Link } from "react-router-dom";
import arrowright from "../../Assets/Images/arrow-right.png";
import Progressbar from "../../Compnents/ProgressBar";
import { FormContxt } from "../../App";
import Form from "react-bootstrap/Form";
import "./Questions.css";

export default function Questions4() {
  let Logo = useRef(null);
  const { stepNum, questionsAns, handleStepNumber, handlequestionsAns } =
    useContext(FormContxt);
  const [ErrorFlag, setErrorFlag] = useState(
    questionsAns[3].Q4 === null ? true : false
  );
  const [NumberErr, setNumberErr] = useState(false);

  useEffect(() => {
    //target , duration ,config

    gsap.from(Logo, {
      duration: 1.5,
      //   delay: 0.5,
      y: 20,
      opacity: 0,
      ease: "power3.inOut",
    });
  }, []);
  return (
    <div className="QuestionContainer">
      <div className="Logo" ref={(el) => (Logo = el)}>
        <Link to="/">XPOVI</Link>
      </div>
      <div className="questionContent">
        <h1 className="font25">Did you have an investment?</h1>
      </div>
      <Container className="flexEvenly margin20">
        <Row>
          <Col
            lg="6"
            md="12"
            className="mt-2"
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <div
              className={
                questionsAns[3].Q4 === "Yes"
                  ? "singleOptionActive"
                  : "singleOption"
              }
              onClick={() => {
                handlequestionsAns("Yes", 4);
                if (questionsAns[4].Q5 === null) {
                  setErrorFlag(true);
                } else {
                  setErrorFlag(false);
                }
              }}
            >
              <h3>Yes</h3>
            </div>
          </Col>
          <Col
            lg="6"
            md="12"
            className="mt-2"
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <div
              className={
                questionsAns[3].Q4 === "No"
                  ? "singleOptionActive"
                  : "singleOption"
              }
              onClick={() => {
                handlequestionsAns("No", 4);
                setErrorFlag(false);
              }}
            >
              <h3>No</h3>
            </div>
          </Col>
        </Row>
      </Container>
      <div
        className={
          questionsAns[3].Q4 === "Yes"
            ? "secondQuestionActive"
            : "secondQuestiondisactive"
        }
      >
        <div className="questionContent ">
          <h1 className="font25">How much was the investment?</h1>
        </div>
        <div className="margin20">
          <Form>
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Control
                type="number"
                min="0"
                disabled={questionsAns[3].Q4 === "Yes" ? false : true}
                style={{ width: "200px", margin: "0 auto" }}
                value={questionsAns[4].Q5 === null ? "" : questionsAns[4].Q5}
                onChange={(e) => {
                  handlequestionsAns(e.target.value, 5);
                }}
                onKeyUp={(event) => {
                  let value = event.target.value;
                  let res = /^(?<=-*)\+?\d*\.?\d+$|^(?<=-*)\+?\d+\.$/.test(
                    value
                  );
                  if (res === false) setNumberErr(true);
                  else {
                    setNumberErr(false);
                    setErrorFlag(false);
                  }
                  return res;
                }}
              />
            </Form.Group>
            {NumberErr === true ? (
              <p className="redColor">Please enter positive numbers only!</p>
            ) : null}
          </Form>
        </div>
      </div>
      <div className="arrowDiv">
        <Link
          to={"/question1"}
          onClick={() => {
            handleStepNumber(stepNum - 1);
          }}
        >
          <img src={arrowright} alt="arrow-right" className="arrow" />
        </Link>
        <Link
          to="/Summary"
          onClick={(event) => {
            if (ErrorFlag === true) {
              event.preventDefault();
            }
          }}
        >
          <span
            className={
              ErrorFlag === true || NumberErr === true
                ? "disablenextSpan"
                : "nextSpan"
            }
            onClick={() => {
              if (ErrorFlag === false) handleStepNumber(stepNum + 1);
            }}
          >
            Next
          </span>
        </Link>
      </div>
      <Progressbar />
    </div>
  );
}
