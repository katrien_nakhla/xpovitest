import React, { useState, useRef, useContext, useEffect } from "react";
import gsap from "gsap";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { Link,Navigate  } from "react-router-dom";
// import { Redirect } from "react-router";
import { FormContxt } from "../../App";
import "../../server";
import Toast from "react-bootstrap/Toast";
import "./Questions.css";

export default function Summary() {
  let Logo = useRef(null);
  const { questionsAns } = useContext(FormContxt);
  const [showToast, setshowToast] = useState(false);
  const [redirectFlag, setredirectFlag] = useState(false);

  useEffect(() => {
    gsap.from(Logo, {
      duration: 1.5,
      y: 20,
      opacity: 0,
      ease: "power3.inOut",
    });
  }, []);

  const submitAnswers = () => {
    fetch("/api/formdata", {
      method: "POST",
      body: JSON.stringify(questionsAns),
    })
      .then((res) => res.json())
      .then((json) => {
        setshowToast(true);
        setredirectFlag(true);
      })
      .catch((err) => {
        // console.log("err-->", err)
      });
  };

  if (redirectFlag === true) {
    return <Navigate  to="/" replace={true} />;
  }
  return (
    <div className="QuestionContainer pb-5">
      <div className="Logo" ref={(el) => (Logo = el)}>
        <Link to="/">XPOVI</Link>
      </div>
      <div
        className="questionContent "
        style={{ marginBottom: "20px", marginTop: "0px" }}
      >
        <h3 className="title">Summary for all your answers</h3>
      </div>
      <Container className="w90" fluid>
        <Row>
          {questionsAns &&
            questionsAns.length > 0 &&
            questionsAns.map((item, index) => {
              return (
                <Col lg="6" md="12" key={index} className="mt-2 ">
                  <div
                    className="singleDivSummary"
                    style={{ border: "2px solid gray" }}
                  >
                    <h3>{item.title}</h3>
                    <p className="answeStyle">
                      {item[`Q${index + 1}`] === null
                        ? "No Answer"
                        : item[`Q${index + 1}`]}
                    </p>
                  </div>
                </Col>
              );
            })}
        </Row>
      </Container>

      <div className="arrowDiv">
        <button
          className={"nextSpan"}
          onClick={() => {
            submitAnswers();
          }}
        >
          Submit
        </button>
      </div>
      <Toast
        onClose={() => setshowToast(false)}
        show={showToast}
        bg={"success"}
        autohide
        style={{ color: "white" }}
      >
        <Toast.Body>Your answer are submitted successfully!</Toast.Body>
      </Toast>
    </div>
  );
}
