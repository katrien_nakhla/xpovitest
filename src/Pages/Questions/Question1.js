import React, { useState, useRef, useContext, useEffect } from "react";
import gsap from "gsap";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { Link } from "react-router-dom";
import arrowright from "../../Assets/Images/arrow-right.png";
import Progressbar from "../../Compnents/ProgressBar";
import { FormContxt } from "../../App";
import Questions2 from "./Question2";
import Questions3 from "./Question3";

import "./Questions.css";

export default function Questions1() {
  let Logo = useRef(null);

  const { stepNum, questionsAns, handleStepNumber, handlequestionsAns } =
    useContext(FormContxt);
  const [ErrorFlag, setErrorFlag] = useState(
    questionsAns[0].Q1 === null ? true : false
  );
  useEffect(() => {
    //target , duration ,config

    gsap.from(Logo, {
      duration: 1.5,
      //   delay: 0.5,
      y: 20,
      opacity: 0,
      ease: "power3.inOut",
    });
  }, []);

  return (
    <div className="QuestionContainer">
      <div className="Logo" ref={(el) => (Logo = el)}>
        <Link to="/">XPOVI</Link>
      </div>
      <div className="questionContent">
        <h1 className="font25">Is your business model B2C or B2B or both?</h1>
      </div>
      <Container className="flexEvenly">
        <Row>
          <Col
            lg="4"
            md="12"
            className="mt-2"
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <div
              className={
                questionsAns[0].Q1 === "B2C"
                  ? "singleOptionActive"
                  : "singleOption"
              }
              onClick={() => {
                handlequestionsAns("B2C", 1);
                setErrorFlag(false);
              }}
            >
              <h3>B2C</h3>
            </div>
          </Col>
          <Col
            lg="4"
            md="12"
            className="mt-2"
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <div
              className={
                questionsAns[0].Q1 === "B2B"
                  ? "singleOptionActive"
                  : "singleOption"
              }
              onClick={() => {
                handlequestionsAns("B2B", 1);
                setErrorFlag(false);
              }}
            >
              <h3>B2B</h3>
            </div>
          </Col>
          <Col
            lg="4"
            md="12"
            className="mt-2"
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <div
              className={
                questionsAns[0].Q1 === "Both"
                  ? "singleOptionActive"
                  : "singleOption"
              }
              onClick={() => {
                handlequestionsAns("Both", 1);
                setErrorFlag(false);
              }}
            >
              <h3>Both</h3>
            </div>
          </Col>
        </Row>
      </Container>

      {questionsAns[0].Q1 !== null ? (
        questionsAns[0].Q1 === "Both" ? (
          <>
            <Questions2 />
            <Questions3 />
          </>
        ) : questionsAns[0].Q1 === "B2B" ? (
          <Questions2 />
        ) : (
          <Questions3 />
        )
      ) : null}

      <div className="arrowDiv">
        <Link
          to={"/question4"}
          onClick={(event) => {
            if (ErrorFlag === true) {
              event.preventDefault();
            }
          }}
        >
          <span
            className={ErrorFlag === true ? "disablenextSpan" : "nextSpan"}
            onClick={() => {
              if (ErrorFlag === false)
                handleStepNumber(
                  questionsAns[0].Q1 === "Both" ? stepNum + 1 : stepNum + 2
                );
            }}
          >
            Next
          </span>
        </Link>
      </div>
      <Progressbar />
    </div>
  );
}
