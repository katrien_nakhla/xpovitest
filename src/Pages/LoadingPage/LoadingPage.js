import React, { useRef, useEffect } from "react";
import gsap from "gsap";
import { Link } from "react-router-dom";

import "./LoadingPage.css";

export default function LoadingPage() {
  let leftContainer = useRef(null);
  let rightContainer = useRef(null);
  let centerContainer = useRef(null);
  let Logo = useRef(null);
  let Info = useRef(null);
  let Story = useRef(null);
  let Menu = useRef(null);
  let liItem = useRef(null);

  useEffect(() => {
    //target , duration ,config
    gsap.from(leftContainer, {
      duration: 2,
      width: "0",
      ease: "power3.inOut",
    });
    gsap.from(rightContainer, {
      duration: 2,
      delay: 1.5,
      opacity: 0,
      width: "0",
      ease: "power3.inOut",
    });
    gsap.from(centerContainer, {
      duration: 2,
      delay: 2.4,
      width: "0",
      x: -20,
      ease: "power3.inOut",
    });
    gsap.from(Logo, {
      duration: 2,
      delay: 1.5,
      y: 20,
      opacity: 0,
      ease: "power3.inOut",
    });
    gsap.from(Info, {
      duration: 2,
      delay: 1.5,
      y: 50,
      opacity: 0,
      scale: 2.5,
      ease: "power3.inOut",
    });
    gsap.from(Story, {
      duration: 2,
      delay: 2.5,
      y: 30,
      opacity: 0,
      ease: "power3.inOut",
    });
    gsap.from(Menu, {
      duration: 2,
      delay: 2,
      y: 20,
      opacity: 0,
      rotation: 90,
      ease: "power3.inOut",
    });
    gsap.from(Menu, {
      duration: 2,
      delay: 2.5,
      y: 20,
      rotation: -90,
      ease: "power3.inOut",
    });
    gsap.to(liItem, {
      duration: 2,
      delay: 3,
      y: 20,
      ease: "power3.inOut",
    });
  });

  return (
    <div className="containers">
      <div className="left-container" ref={(el) => (leftContainer = el)}>
        <div className="Logo" ref={(el) => (Logo = el)}>
          XPOVI
        </div>
        <div className="Info" ref={(el) => (Info = el)}>
          <span>FAQ</span>
          <span>Help</span>
        </div>
      </div>
      <div className="right-container" ref={(el) => (rightContainer = el)}>
        <div className="first-block"></div>
        <div className="menu-container">
          <div className="menu" ref={(el) => (Menu = el)}>
            <span></span>
            <span></span>
            <span></span>
          </div>
        </div>
        <div className="Story" ref={(el) => (Story = el)}>
          <h1>Financial Plan</h1>
          <p>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has
          </p>
          <Link to="/question1" className="link">
            <div className="createOne">Let's Create one!</div>
          </Link>
        </div>
        <div className="socialContainer">
          <ul ref={(el) => (liItem = el)}>
            <li>
              <a
                href="https://www.facebook.com/Xpovi.solutions/"
                target="_blank"
              >
                Facebook
              </a>
            </li>
            <li>
              <a
                href="https://www.instagram.com/xpovi.solutions/"
                target="_blank"
              >
                Instagram
              </a>
            </li>
          </ul>
        </div>
      </div>
      <div
        className="CenterContainer"
        ref={(el) => (centerContainer = el)}
      ></div>
    </div>
  );
}
