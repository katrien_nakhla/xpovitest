import React, { useContext } from "react";
import ProgressBar from "react-bootstrap/ProgressBar";
import { FormContxt } from "../App";
import "./ProgressBar.css";

export default function Progressbar() {
  const { stepNum } = useContext(FormContxt);
  const now = stepNum * 50;

  return (
    <div className="progressBarDiv">
      <ProgressBar now={now} label={`${now}%`} />
    </div>
  );
}
