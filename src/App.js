import React, { createContext, useState } from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import LoadingPage from "./Pages/LoadingPage/LoadingPage";
import Question1 from "./Pages/Questions/Question1";
import Question2 from "./Pages/Questions/Question2";
import Question3 from "./Pages/Questions/Question3";
import Question4 from "./Pages/Questions/Question4";
import Summary from "./Pages/Questions/Summary";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

export const FormContxt = createContext();

function App() {
  const [stepNum, setStepNum] = useState(1);
  const [questionsAns, setQuestion1Ans] = useState([
    { Q1: null, title: "Is your business model B2C or B2B or both?" },
    { Q2: null, title: "Do you target all age brackets?" },
    { Q3: null, title: "Do you target all industries?" },
    { Q4: null, title: "Did you have an investment?" },
    { Q5: null, title: "How much was the investment?" },
  ]);

  const handleStepNumber = (number) => {
    setStepNum(number);
  };
  const handlequestionsAns = (ans, questionNumber) => {
    let copyArr = [...questionsAns];

    copyArr[questionNumber - 1][`Q${questionNumber}`] = ans;
    if (questionNumber === 4 && ans === "No") {
      copyArr[4].Q5 = null;
    }
    setQuestion1Ans(copyArr);
  };

  return (
    <FormContxt.Provider
      value={{
        stepNum: stepNum,
        questionsAns: questionsAns,
        handleStepNumber: handleStepNumber,
        handlequestionsAns: handlequestionsAns,
      }}
    >
      <Router>
        <div className="App">
          <div className="content-container">
            <div className="wrapper">
              <div className="home">
                <Routes>
                  <Route exact path="/" element={<LoadingPage />} />
                  <Route exact path="/question1" element={<Question1 />} />
                  <Route exact path="/question2" element={<Question2 />} />
                  <Route exact path="/question3" element={<Question3 />} />
                  <Route exact path="/question4" element={<Question4 />} />
                  <Route exact path="/Summary" element={<Summary />} />
                </Routes>
              </div>
            </div>
          </div>
        </div>{" "}
      </Router>
    </FormContxt.Provider>
  );
}

export default App;
